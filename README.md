# JS GAME - Goose Game

## Project

*Project Duration : 3 weeks*

Learner at Simplon.co in Lyon, our teachers asked to each of us to code a game with HTML, CSS and Javascript, and without any framework. As we have just learn how to do simple loops and conditions, I've looking for a simple game in an actual theme to improve it.



## Description

The goose game is an old game that I used to play with my grandmother ;)

 Here, I've made an adaptation of this famous board game, based on TV show 'La Casa de Papel'.

The goal is to precisely reach the finish box by matching the dice score. When playing, some events are programming to happen on the most of the cases. Those events are explained in the 'Rules' section (at the bottom of the board game). You have to finish with enough cash to be free (and rich) !!



## User stories :

1. As an user, I want to play a one-player game to pass through time.
2. As a learner, I want to code a pure Javascript Game to improve my skills.
3. As a recruiter, I want to know about he skills of a candidate to be sure he has the basics of programming languages.
4. As a teacher, I want to challenge my students to have the better of their imagination ;)



## Issues :

- 1st week :  Find ~~an amazing,~~ ~~a good,~~ an idea.
  - Must be a simple game to be code in pure JS with my beginner skills (loop, condition, event listener, create element).
  - Drawing a model, make the files structure and import images.d
  - *Sprint* :  Coding the files .html and .css.
- 2nd week : Coding the game functionalities :
  - Making the player move on the dice rolling according to it's value.
  - Add events on the boxes. 
  - *Sprint* : Prevent the player to finish if not exactly at the finish case.
- 3rd week : Make it more easier to use.
  - Recast the CSS to have more visibility on the player.
  - Make Call To Action buttons.
  - *Sprint :* Make it test by others to improve the game according to their advices.



## Visuals

### Functional Model :

![](./img/models-and-diagrams/maquette-goose-game.jpg)



### UML - Use Case Diagram :

![](./img/models-and-diagrams/UseCaseDiagram1.jpg)

### Screenshots :

![](/var/www/html/projets-simplon/projet-JS/img/models-and-diagrams/JS-goose-game0.png)

![](/var/www/html/projets-simplon/projet-JS/img/models-and-diagrams/JS-goose-game3.png)

![](/var/www/html/projets-simplon/projet-JS/img/models-and-diagrams/JS-goose-game2.png)



### Demonstration video :

https://www.loom.com/share/6706286a97af430bac049a1aca9775dc



## Code explanation

### 1st Step : Drawing the game board

I used CSS Grid in order to have a cochlea game board (snail-shaped)

```css
grid-template-areas: 
"Start box1 box2 box3 box4 box5"
"box15 box16 box17 box18 box19 box6"
"box14  Finish Finish box21 box20 box7"
"box13 box12 box11 box10 box9 box8";
```



### 2nd Step : Create a player

I chose to create a 'div' element in my JS's file, and to add a CSS class to it with the player as a background image.

```javascript
let playerDiv = document.createElement('div');
playerDiv.classList.add('player');
```

```css
.player {
    width: inherit;
    border: none;
    border-radius: none;
    height: 90%;
    background: center / 80% no-repeat url(../img/little-guy.png);
}
```



### 3rd Step : Make the player moving according to the dice roll

First, I randomize the value of the dice (from 1 to 6), and return a dice's image according to the roll value.

```javascript
 let faceValue = Math.floor((Math.random() * 6) + 1);
        diceImg.src = "./img/dice/dice" + faceValue + ".png";
```

Then, I catch the box where the player is :

```javascript
let playerBox = document.querySelector('#game-board div .player');
actualBox = playerBox.parentNode;

```

And I remove the player from the box where it was previously :

```javascript
playerDiv.parentNode.removeChild(playerDiv);
```

Then, I calculate the value of the new box according to the previous one and the roll value :

```javascript
let numBox = actualBoxValue.slice(actualBoxValue.length - 2);
newBoxToReach = parseInt(numBox) + faceValue;

```

As the boxes classes have specified numbers, I need to verify the value of the moving box and then add the class '.player' to it :

```javascript
if (newBoxToReach < 10) {
	movinBox = document.querySelector('.box-0' + newBoxToReach);
    } else {
        movinBox = document.querySelector('.box-' + newBoxToReach);
    }

movinBox.appendChild(playerDiv);
```

### 4th Step : add events on most of the boxes

Examples for the first two events :

```javascript
 if (newBoxToReach === 1) {
            alert('Case 1 : Tu n\'as pas trouvé de team, retourne sur la case départ');
            newBoxToReach = 0;
} else if (newBoxToReach === 2) {
            alert('Case 2 : Tu as trouvé ta team, bravo ! Tu avances case 3 et gagne 100 000 €');
            cash += 100000;
            newBoxToReach = 3;}
```

### 5th Step : 1. Make the game finishing

Player can only finish arriving exactly on the finish box, and there are 2 ways : if player's cash is positive, a winner page gives his score (cash and turns), or else, it's a looser page :

```javascript
else if (newBoxToReach === 22) {

            newBoxToReach = 22;
            rollBtn.classList.remove('blink');

            success = true;

            board.style.display = 'none';
            let main = document.querySelector('main');

            if (cash > 0) {
                let winner = document.createElement('article');
                winner.innerHTML = `<h2>Bravo !!!</h2> <p>Tu as réussi en <strong>${turns} tours</strong>,</p><p> Et tu as gagné <strong>${cash} €</strong><small> (mais que dans le jeu hein !;)</small></p>`;
                main.appendChild(winner);
                playBtn.addEventListener('click', function() {
                    window.location.reload();
                });
            } else {
                let looser = document.createElement('article');
                looser.innerHTML = `<h2>Dommage !</h2> <p>Tu es sorti vivant après <strong>${turns} tours</strong>, mais tu as perdu <strong>${-cash} €</strong><small>(Paiements acceptés : Espèces, Chèques, CB, Virement Bancaire, PayPal & Bitcoins)</small></p>`;
                main.appendChild(looser);
                playBtn.addEventListener('click', function() {
                    window.location.reload();
                });
            }

```

### 5th Step : 2. Provide the player to finish if it doesn't reach exactly the finish case

```javascript
if (newBoxToReach > 22) {
	if (newBoxToReach === 23) {
    	newBoxToReach = 21;
    } else if (newBoxToReach === 24) {
   		newBoxToReach = 20;
    } else if (newBoxToReach === 25) {
        newBoxToReach = 19;
    } else if (newBoxToReach === 26) {
        newBoxToReach = 18;
    } else if (newBoxToReach === 27) {
        newBoxToReach = 17;
    }
};
```

### 5th Step : 3. Display the winning/loosing page

```javascript
if (cash > 0) {
	let winner = document.createElement('article');
	winner.innerHTML = `<h2>Bravo !!!</h2> <p>Tu as réussi en <strong>${turns} tours</strong>,</p><p> Et tu as gagné <strong>${cash} €</strong><small> (mais que dans le jeu hein !;)</small></p>`;
	main.appendChild(winner);
	playBtn.addEventListener('click', function() {
	window.location.reload();
	});
} else {
	let looser = document.createElement('article');
	looser.innerHTML = `<h2>Dommage !</h2> <p>Tu es sorti vivant après <strong>${turns} tours</strong>, mais tu as perdu <strong>${-cash} €</strong><small>(Paiements acceptés : Espèces, Chèques, CB, Virement Bancaire, PayPal & Bitcoins)</small></p>`;
	main.appendChild(looser);
	playBtn.addEventListener('click', function() {
     window.location.reload();
     });
}
```



## Support

Please get in touch if you want to report any bug or error : soph.brunier@gmail.com