//Variables Selectors for dice and buttons
let board = document.querySelector('#game-board');
let rollBtn = document.querySelector('#roll-btn');
let diceImg = document.querySelector('#dice');
let playBtn = document.querySelector('#play-btn');
let startBox = document.querySelector('.box-00');
let boxes = document.querySelectorAll('div');
let turns;
let cash;
let success = false;

//Make the play Button blink before playing
playBtn.classList.add('blink');

//Dice images
let dice1 = new Image().src = './img/dice/dice1.png';
let dice2 = new Image().src = './img/dice/dice2.png';
let dice3 = new Image().src = './img/dice/dice3.png';
let dice4 = new Image().src = './img/dice/dice4.png';
let dice5 = new Image().src = './img/dice/dice5.png';
let dice6 = new Image().src = './img/dice/dice6.png';

//create the div.player
let playerDiv = document.createElement('div');
playerDiv.classList.add('player');

// Then the game (because it only starts when clicking on New Game button)
playBtn.addEventListener('click', function() {
    // Stop the lightning on play-btn and put it on roll-btn
    playBtn.classList.remove('blink');
    rollBtn.classList.add('blink');

    let actualBox = startBox;
    // let newBoxToReach = 0;
    cash = 0;
    turns = 0;
    actualBox.appendChild(playerDiv); // Place the div.player on the board when newGame Btn is pressed

});

//Function to roll the dice when clicking on the "Roll" button
rollBtn.addEventListener('click', function() {
    // si success = true on fait un return dans if pour bloquer le jeu
    if (success) {
        return;
    } else {

        let faceValue = Math.floor((Math.random() * 6) + 1);
        diceImg.src = "./img/dice/dice" + faceValue + ".png";
        turns += 1;

        // 1.on va stocker la case actuelle dans une variable
        // 2.on enleve la div avec la classe "player" à la case actuelle
        // 2bis. afficher bg si disparu (ou changement bg si player a la place)
        // 3.on calcule la case d'arrivée (case actuelle + dice.faceValue) qu'on met dans une variable
        // 4.on ajoute la classe player sur la case d'arrivée

        let playerBox = document.querySelector('#game-board div .player'); // Selectionne la div.player

        // 2e selecteur necessaire car 2 changements

        actualBox = playerBox.parentNode; // Selectionne la div actuelle (parent de la div.player)

        playerDiv.parentNode.removeChild(playerDiv); // On enlève le player de sa 1ère div
        actualBoxValue = actualBox.classList.value; // On calcule le numéro de la box 1ere div

        // On calcule le numéro de la box actuelle

        let numBox = actualBoxValue.slice(actualBoxValue.length - 2);

        // on ajoute la valeur du dé à la box actuelle pour atteindre la nouvelle box
        newBoxToReach = parseInt(numBox) + faceValue;


        // Quand on arrive sur la case 22, on repart en arrière
        if (newBoxToReach > 22) {
            if (newBoxToReach === 23) {
                newBoxToReach = 21;
            } else if (newBoxToReach === 24) {
                newBoxToReach = 20;
            } else if (newBoxToReach === 25) {
                newBoxToReach = 19;
            } else if (newBoxToReach === 26) {
                newBoxToReach = 18;
            } else if (newBoxToReach === 27) {
                newBoxToReach = 17;
            }
        }

        // on ajoute des évènements
        if (newBoxToReach === 1) {
            alert('Case 1 : Tu n\'as pas trouvé de team, retourne sur la case départ');
            newBoxToReach = 0;

        } else if (newBoxToReach === 2) {
            alert('Case 2 : Tu as trouvé ta team, bravo ! Tu avances case 3 et gagne 100 000 €');
            cash += 100000;
            newBoxToReach = 3;

        } else if (newBoxToReach === 4) {
            alert('Case 4 : Moscou a repéré les failles pour creuser le tunnel, tu empoches 10 000 €');
            cash += 10000;

        } else if (newBoxToReach === 5) {
            alert('Case 5 : La police entre dans le batiment ! Tu retournes case 3 et perd 20 000 €');
            cash -= 20000;
            newBoxToReach = 3;

        } else if (newBoxToReach === 6) {
            alert('Case 6 : Tu repousses l\'attaque de la police! Tu avances à la case 9 et gagne 50 000 €');
            cash += 50000;
            newBoxToReach = 9;

        } else if (newBoxToReach === 7) {
            alert('Case 7 : Tes mauvaises décisions poussent le professeur à sortir de sa cachette, retournes case 4, tu perds 40 000 €');
            cash -= 40000;
            newBoxToReach = 4;

        } else if (newBoxToReach === 8) {
            alert('Case 8 : Nairobi a failli craquer, tout rentre dans l\'ordre mais tu perds 20 000 €');
            cash -= 20000;
            newBoxToReach = 8;

        } else if (newBoxToReach === 10) {

            alert('Case 10 : Palerme n\'est pas content de tes décisions, retournes case 8 et donnes lui 25 000 €');
            cash -= 25000;
            newBoxToReach = 8;

        } else if (newBoxToReach === 12) {

            alert('Case 12 : Denver s\'éclate dans les billets mais en perd quelques-uns, moins 10 000 € !');
            cash -= 10000;

        } else if (newBoxToReach === 13) {

            alert('Case 13 : Un otage (ce petit con d\'Arturo) se rebelle! Tu retourne en case 11 et perds 20 000 €');
            cash -= 20000;
            newBoxToReach = 11;

        } else if (newBoxToReach === 14) {

            alert('Case 14 : Grâce aux bons calculs de Nairobi, tu gagnes 30 000 € en bonus');
            cash += 30000;
            newBoxToReach = 14;

        } else if (newBoxToReach === 15) {

            alert('Case 15 : Le tunnel est bientot prêt, avances case 16 et prends 10 000 € au passage');
            cash += 10000;
            newBoxToReach = 16;

        } else if (newBoxToReach === 18) {

            alert('Case 18 : Lisbonne a rejoint tes rangs, avance case 19 et encaisse 20 000 €');
            cash += 20000;
            newBoxToReach = 19;

        } else if (newBoxToReach === 20) {

            alert('Case 20 : Rio partage sa part avec toi, tu gagnes 30 000 € de plus');
            cash += 30000;

        } else if (newBoxToReach === 21) {

            alert('Case 21 : Denver s\'est amouraché d\'une otage, tu perds 15 000 €');
            cash -= 15000;

        } else if (newBoxToReach === 22) {

            newBoxToReach = 22;
            rollBtn.classList.remove('blink');

            success = true;

            board.style.display = 'none';
            let main = document.querySelector('main');

            if (cash > 0) {
                let winner = document.createElement('article');
                winner.innerHTML = `<h2>Bravo !!!</h2> <p>Tu as réussi en <strong>${turns} tours</strong>,</p><p> Et tu as gagné <strong>${cash} €</strong><small> (mais que dans le jeu hein !;)</small></p>`;
                main.appendChild(winner);
                playBtn.addEventListener('click', function() {
                    window.location.reload();
                });
            } else {
                let looser = document.createElement('article');
                looser.innerHTML = `<h2>Dommage !</h2> <p>Tu es sorti vivant après <strong>${turns} tours</strong>, mais tu as perdu <strong>${-cash} €</strong><small>(Paiements acceptés : Espèces, Chèques, CB, Virement Bancaire, PayPal & Bitcoins)</small></p>`;
                main.appendChild(looser);
                playBtn.addEventListener('click', function() {
                    window.location.reload();
                });
            }

        }



        // on calcule la classe box-?? de la nouvelle case
        if (newBoxToReach < 10) {
            movinBox = document.querySelector('.box-0' + newBoxToReach);
        } else {
            movinBox = document.querySelector('.box-' + newBoxToReach);

        }

        //puis on ajoute le player
        movinBox.appendChild(playerDiv);


        // On affiche le montant cash dans sa div
        let innerCash = document.querySelector('.cash');
        innerCash.innerHTML = `<strong>CASH</strong> :<br> ${cash} €`;

    }
});